package DB

import (
	"Assignment2/structures"
	"fmt"
	"strings"
	"time"
)

/*
This function adds policies to the DB under policies-collection
@param {structures.CountryPolicy} CountryPolicy is the country-policy data that will be stored in the DB
*/
func AddPolicyToDB(countryPolicy structures.CountryPolicy) {
	fmt.Println("Attempting to add policy regarding the following country code: " + countryPolicy.Country_code + " to the DB")

	document, _, err := client.Collection(structures.PoliciesCollection).Add(ctx, countryPolicy)
	if err != nil {
		// Error handling
		fmt.Printf("Failed! Error when adding policy to DB, Error: " + err.Error())
		return
	}

	fmt.Println("Successful! Policy added to collection. Identifier of returned document: " + document.ID)
}

/*
This function checks if a policy for given countryCode exists in the DB
@param {string} countryCode is the iso3 alpha code of a country
@param {string} date is the scope date for policy information
@return {bool} returns true/false if the policy exists in DB
@return {structures.CountryPolicy} returns policy data in the form of the CountryCases struct.
*/
func CheckPolicies(countryCode string, date string) (bool, structures.CountryPolicy) {
	var policies structures.CountryPolicy
	getAllPolicies, err := client.Collection(structures.PoliciesCollection).Documents(ctx).GetAll()
	if err != nil {
		fmt.Println("A error occured while trying to retrieve all policies in the DB. Error:", err.Error())
		return false, structures.CountryPolicy{} //returning false indicating no policy found in DB and an empty CountryPolicy struct
	}

	//Going through all policies stored in the policies collection to check if the case corresponding to the given country name is already available in DB.
	for _, v := range getAllPolicies {
		err = v.DataTo(&policies)
		if err != nil {
			fmt.Println("Failed to add the data of policy: " + v.Ref.ID + "into the webhook struct")
		}

		//hourSince is a variable that checks the time in hour since the policies were stored in the DB
		hourSince := int(time.Since(v.CreateTime.UTC()).Hours())

		//If any stored policy has been there for more than 48 hours (2 days), it will get purged from the DB
		if hourSince > structures.PurgeTime {
			fmt.Println("Attempting to delete the policy data with id: " + v.Ref.ID)
			_, err := v.Ref.Delete(ctx)
			if err != nil { //Error handling if the deleting fails
				fmt.Println("Failed to delete the policy data with the id: " + v.Ref.ID)
			}
			fmt.Println("Successful with deleting the policy data with the id: " + v.Ref.ID)
		}
		//Checking if the given country code and date matches country codes and date stored in the DB
		if strings.ToLower(countryCode) == strings.ToLower(policies.Country_code) && date == policies.Scope {
			return true, policies //returning true indicating that the policy was found in DB and the CountryPolicy struct with the policy data
		}
	}

	return false, structures.CountryPolicy{} //returning false indicating no policy found in DB and an empty CountryPolicy struct
}
