package DB

import (
	"Assignment2/structures"
	"Assignment2/utilities"
	"bytes"
	"cloud.google.com/go/firestore"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var SignatureKey = "X-SIGNATURE"

var Secret []byte

/*
This function gets all the webhooks stored in the DB
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@return []structures.WebhookRegistration returns a list/array of all the webhooks in the form of the webhookregistration struct.
*/
func GetMultipleWebhooks(w http.ResponseWriter) []structures.WebhookRegistration {
	var webhook structures.WebhookRegistration
	var webhooksList []structures.WebhookRegistration
	getAllWebhooks, err := client.Collection(structures.WebhookCollection).Documents(ctx).GetAll()

	if err != nil {
		http.Error(w, "A error occured while trying to retrieve all webhooks", http.StatusInternalServerError)
		return nil
	}

	//Adding all the webhook data into a list
	for _, v := range getAllWebhooks {
		err = v.DataTo(&webhook)
		if err != nil {
			id := v.Data()["Webhook_id"].(string)
			fmt.Println("Failed to add the data of webhook: " + id + "into the webhook struct")
		}

		webhooksList = append(webhooksList, webhook)
	}
	return webhooksList
}

// GetSingleWebhook
//This function gets a single webhook based on the given webhookID
//@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
//@param {string} webhookID is the ID of the webhook
//@return structures.WebhookRegistration returns a single webhooks the form of the webhookregistration struct./*
func GetSingleWebhook(w http.ResponseWriter, webhookID string) structures.WebhookRegistration {
	var webhook structures.WebhookRegistration
	getWebhook, err := client.Collection(structures.WebhookCollection).Doc(webhookID).Get(ctx)
	if err != nil {
		http.Error(w, "Could not find a webhook with the given ID: "+webhookID, http.StatusNotFound)
		//Returning a WebhookRegistration struct that is empty
		return structures.WebhookRegistration{}
	}
	err = getWebhook.DataTo(&webhook)
	if err != nil {
		id := getWebhook.Data()["Webhook_id"].(string)
		fmt.Println("Failed to add the data of webhook: " + id + "into the webhook struct")
	}

	return webhook
}

/*
This function adds a webhook to the firestore DB
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {http.Request} r is the HTTP request made by a client.
*/
func AddWebhooks(w http.ResponseWriter, r *http.Request) {
	var webhook structures.WebhookRegistration
	err := json.NewDecoder(r.Body).Decode(&webhook)
	if err != nil {
		http.Error(w, "Something went wrong: "+err.Error(), http.StatusBadRequest)
	}
	var countryName string
	if len(webhook.Country) == 3 {
		countryName = utilities.GetCountryNamesByAlphaCode(w, webhook.Country)
		//Checking if the country name is not empty which happens when the given alphacode is invalid
		if countryName == "" {
			http.Error(w, "The given ISO 3166-1 alpha-3 country code is invalid. Please give a valid one", http.StatusBadRequest)
			return
		}
	} else {
		//Checking if the given country name is valid
		if !utilities.CheckIfCountryNameIsValid(w, webhook.Country) {
			http.Error(w, "The given country name is invalid. Please give a valid one", http.StatusBadRequest)
			return
		}
		countryName = webhook.Country

	}
	document := client.Collection(structures.WebhookCollection).NewDoc()
	//Converting the country name to lower cases then to a title format
	webhook.Country = strings.Title(strings.ToLower(countryName))
	webhook.Webhook_id = document.ID
	webhook.Registered_calls = 0
	_, err = document.Set(ctx, webhook)
	if err != nil {
		http.Error(w, "Error when adding webhook. Error: "+err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Println("Webhook added to collection. Identifier of returned document: " + document.ID)

	//Returning the webhook id in body that matches the body example given in the assignment
	webhookID := structures.WebhookID{}
	webhookID.Webhook_id = document.ID

	output, err := json.Marshal(webhookID)
	if err != nil {
		fmt.Println("Error when converting WebhookRegistration into JSON data. Error: " + err.Error())
		return
	}
	http.Error(w, string(output), http.StatusCreated)
}

/*
This function deletes a webhook based on the given webhookID from the DB
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {string} webhookID is the ID of the webhook
*/
func DeleteWebhook(w http.ResponseWriter, webhookID string) {
	_, err := client.Collection(structures.WebhookCollection).Doc(webhookID).Delete(ctx)
	if err != nil {
		http.Error(w, "The webhook associated with the following webhook ID: "+webhookID+" could not be deleted", http.StatusNotFound)
		return
	}
}

/*
This function registeres all the service invokes on cases and policy endpoints and triggeres the webhook if conditions are met
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {string} is the country that the user invoked either in cases or policy endpoints
*/
func ServiceInvoke(w http.ResponseWriter, country string) {
	webhookList := GetMultipleWebhooks(w)
	//Going through the webhookList to check if the given country name matches a country name in the webhookList
	for _, v := range webhookList {
		if v.Country == country {

			v.Registered_calls += 1
			//Updates the registered calls of the webhook in DB
			UpdateWebhookRegisteredCalls(v.Webhook_id, v.Registered_calls)
			//If the registered calls is more than the noted calls (which is the minimum number of repeated invocations) then the webhook will trigger
			if v.Registered_calls >= v.Calls {

				//Triggering the webhook
				go TriggerWebhook(v.Url, v)
				//Resetting Registered calls
				UpdateWebhookRegisteredCalls(v.Webhook_id, 0)
				log.Println("Resetting the registered call of webhook with the ID: " + v.Webhook_id)
			}
		}
	}
}

/*
This function updates the registerd calls of a webhook in the DB
@param {string} webhookID is the ID of the webhook
@param {int} RegisteredCalls is the number of service invokes in the cases or policy endpoints
*/
func UpdateWebhookRegisteredCalls(WebhookID string, RegisteredCalls int) {
	_, err := client.Collection(structures.WebhookCollection).Doc(WebhookID).Update(ctx, []firestore.Update{
		{Path: "Registered_calls", Value: RegisteredCalls},
	})
	if err != nil {
		fmt.Errorf("Had a problem updating the following webhook: " + WebhookID)
	}
}

/*
This function triggeres the webhook by sending the webhook data to the URL written in the webhook
@param {string} url is the URL to be triggered upon event (the service that should be invoked)
@param {structures.WebhookRegistration} content is the webhook data
*/
func TriggerWebhook(url string, content structures.WebhookRegistration) {

	json, err := json.Marshal(content)
	if err != nil {
		fmt.Errorf("Error when converting WebhookRegistration into JSON data. Error: " + err.Error())
		return
	}
	fmt.Println("Attempting invocation of url " + url + " with content '" + string(json) + "'.")
	req, err := http.NewRequest("POST", url, bytes.NewReader(json))
	if err != nil {
		log.Printf("%v", "Error during request creation. Error:", err)
		return
	}
	// Hash content (for content-based validation; not relevant for URL-based validation)
	mac := hmac.New(sha256.New, Secret)
	_, err = mac.Write(json)
	if err != nil {
		log.Printf("%v", "Error during content hashing. Error:", err)
		return
	}
	// Convert hash to string & add to header to transport to client for validation
	req.Header.Add(SignatureKey, hex.EncodeToString(mac.Sum(nil)))

	// Perform invocation
	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Println("Error in HTTP request. Error:", err)
		return
	}

	// Read the response
	response, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println("Something is wrong with invocation response. Error:", err)
		return
	}

	fmt.Println("Webhook invoked. Received status code " + strconv.Itoa(res.StatusCode) +
		" and body: " + string(response))
}
