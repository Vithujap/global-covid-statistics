package DB

import (
	"cloud.google.com/go/firestore"
	"context"
	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
	"log"
)

var ctx context.Context
var client *firestore.Client

/*
This function initilizes the firestore DB
*/
func FirestoreInit() {
	// Firebase initialisation
	ctx = context.Background()
	sa := option.WithCredentialsFile("C:\\Users\\vithu\\OneDrive\\Razer_PC\\University\\Cloud Technologies\\Assignments\\Assignment2\\prog2205-assignment2-firebase-adminsdk-tf7t8-885bb5ca07.json")
	app, err := firebase.NewApp(ctx, nil, sa)
	if err != nil {
		log.Fatalln(err)
	}

	// Instantiate client
	client, err = app.Firestore(ctx)

	// Alternative setup, directly through Firestore (without initial reference to Firebase); but requires Project ID
	// client, err := firestore.NewClient(ctx, projectID)

	if err != nil {
		log.Fatalln(err)
	}

}

/*
This function closes the firestore DB
*/
func FirestoreClose() {
	// Close down client
	defer func() {
		err := client.Close()
		if err != nil {
			log.Fatal("Closing of the firebase client failed. Error:", err)
		}
	}()
}
