package DB

import (
	"Assignment2/structures"
	"fmt"
	"time"
)

/*
This function adds cases to the DB under cases-collection
@param {structures.CountryCases} countryCases is the country-cases data that will be stored in the DB
*/
func AddCaseToDB(countryCases structures.CountryCases) {
	fmt.Println("Attempting to add case regarding the following country: " + countryCases.Country + " to the DB")
	//Adding case data to DB
	document, _, err := client.Collection(structures.CasesCollection).Add(ctx, countryCases)
	if err != nil {
		// Error handling
		fmt.Printf("Failed! Error when adding case to DB, Error: " + err.Error())
		return
	}

	fmt.Println("Successful! Case added to collection. Identifier of returned document: " + document.ID)
}

/*
This function checks if a case for given countryname exists in the DB
@param {string} countryName is the name of a country
@return {bool} returns true/false if the case exists in DB
@return {structures.CountryCases} returns case data in the form of the CountryCases struct.
*/
func CheckCases(countryName string) (bool, structures.CountryCases) {
	var cases structures.CountryCases
	getAllCases, err := client.Collection(structures.CasesCollection).Documents(ctx).GetAll()
	if err != nil {
		fmt.Println("A error occured while trying to retrieve all cases in the DB. Error:", err.Error())
		return false, structures.CountryCases{} //returning false to indicate no cases was found in DB and an empty countryCases struct
	}

	//Going through all cases stored in the cases collection to check if the case corresponding to the given country name is already available in DB.
	for _, v := range getAllCases {
		err = v.DataTo(&cases)
		if err != nil {
			fmt.Println("Failed to add the data of cases: " + v.Ref.ID + "into the webhook struct")
		}
		//hourSince is a variable that checks the time in hour since the cases was stored in the DB
		hourSince := int(time.Since(v.CreateTime.UTC()).Hours())

		//If the stored case has been there for more than 48 hours, it will get purged from the DB
		if hourSince > structures.PurgeTime {
			fmt.Println("Attempting to delete the case data with id: " + v.Ref.ID)
			_, err := v.Ref.Delete(ctx)
			if err != nil { //Error handling if the deleting fails
				fmt.Println("Failed to delete the case data with the id: " + v.Ref.ID)
			}
			fmt.Println("Successful with deleting the case data with the id: " + v.Ref.ID)
		}
		//Checking if the given country name matches country names stored in the DB
		if countryName == cases.Country {
			return true, cases //returning true indicating that the case was found in DB and the case struct with the case data
		}
	}

	return false, structures.CountryCases{} //returning false to indicate no cases was found in DB and an empty countryCases struct
}
