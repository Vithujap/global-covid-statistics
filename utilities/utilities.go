package utilities

import (
	"Assignment2/structures"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

/*
This function is used to get a country name based on the given ISO3 alpha code
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {string} alphacode is the ISO 3166-1 alpha-3 code of a country.
@return {string} returns the country name
*/
func GetCountryNamesByAlphaCode(w http.ResponseWriter, alphaCode string) string {

	//Invoke external request to the university API to get the data
	resCountry, err := ExternalRequest(w, http.MethodGet, "https://restcountries.com/v3.1/alpha?codes="+alphaCode+"&fields=name", nil)
	if err != nil {
		fmt.Errorf(err.Error())

	}
	//reading data from the resUniversity http response
	countryOutput, err := ioutil.ReadAll(resCountry.Body)
	if err != nil {
		fmt.Errorf("Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)

	}

	var countryName []structures.CountryName

	defer resCountry.Body.Close()

	json.Unmarshal(countryOutput, &countryName)

	//Controlling when the given alphacode is not valid
	if len(countryName) == 0 {
		fmt.Errorf("Cant find any countries with the given alphacode")
		return ""
	}

	return strings.Title(strings.ToLower(countryName[0].Name["common"]))
}

/*
This function is used to check if the given country-name is valid
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {string} country is the written name of a country by the user
@return {bool} returns true/false if the given country name is valid
*/
func CheckIfCountryNameIsValid(w http.ResponseWriter, country string) bool {
	//Invoke external request to the university API to get the data
	resCountry, err := ExternalRequest(w, http.MethodGet, "https://restcountries.com/v3.1/name/"+country+"?fullText=true", nil)
	if err != nil {
		fmt.Errorf(err.Error())
	}
	//reading data from the resUniversity http response
	countryOutput, err := ioutil.ReadAll(resCountry.Body)
	if err != nil {
		fmt.Errorf("Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
	}
	var countryName []structures.CountryName
	defer resCountry.Body.Close()

	json.Unmarshal(countryOutput, &countryName)
	if len(countryName) == 0 {
		log.Println("Countryname is not valid")
		return false
	}
	return true
}

/*
This function is used to get data from external API
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {string} method is the http method that is used http request
@param {url} url is the URL of the external API
@param {[]byte} jsonvalue is the JSON data used for post requests
@return {http.Response} returns the HTTP request
@return {error} returns any potential error
*/
func ExternalRequest(w http.ResponseWriter, method string, url string, jsonValue []byte) (*http.Response, error) {
	//Request to the external  API to get the data
	r2, err := http.NewRequest(method, url, bytes.NewBuffer(jsonValue))

	if err != nil {
		http.Error(w, "Error in creating request. Error code: "+err.Error(), http.StatusInternalServerError)
		return nil, err
	}

	// Instantiate the client
	client := &http.Client{}
	//Issue request
	res, err := client.Do(r2)

	if err != nil {
		http.Error(w, "Error in response. Error message: "+err.Error(), res.StatusCode)
		return nil, err
	}

	return res, nil

}
