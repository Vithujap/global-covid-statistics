package handlers

import (
	"Assignment2/DB"
	"Assignment2/structures"
	"Assignment2/utilities"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

/*
This function is the handler for the cases endpoint. It only handles GET requests and retrieves case information regarding a country from -
either DB or external API,and sends the case information as an http response.
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {http.Request} r is the HTTP request made by a client.
*/
func CaseHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodGet:

		//Prepare response header
		w.Header().Add("content-type", "application/json")
		//Splitting up the URL
		split := strings.Split(r.URL.Path, "/")
		var countryName string
		//Checking if the user input is a country name or a ISO 3166-1 alpha-3 country code
		if len(split[4]) == 3 {
			countryName = utilities.GetCountryNamesByAlphaCode(w, split[4])
		} else {
			countryName = split[4]
		}

		//Checking if the url path matches the one that is used for testing. If not use URL for external API
		if r.URL.Path == structures.STUB_CASES_PATH {
			fmt.Println("URL matches the one for testing. Stubbing instead of using external API")
			testURL := r.URL.String()
			method := http.MethodGet
			getCaseInformationFromExternalAPI(w, countryName, testURL, method, true) //the information recieved will be from stubbed not from external api
			return
		}
		//Converting the country name to lower cases then to a title format because the external API is case-sensitive
		countryName = strings.Title(strings.ToLower(countryName))

		//Checking if the case data for the given country is already available in the DB.
		caseExists, countryCases := DB.CheckCases(countryName)
		if caseExists {
			output, err := json.Marshal(countryCases)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Printf("Failed when encoding countryCases struct. Error: " + err.Error())
				return
			}
			fmt.Println("Case found in the DB. Getting case data from the DB")
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, string(output))
		} else {
			fmt.Println("Case was not found in DB. Getting case data from external API")
			externalAPIURL := "https://covid19-graphql.vercel.app/"
			method := http.MethodPost
			getCaseInformationFromExternalAPI(w, countryName, externalAPIURL, method, false) //Get case data from external API
		}

		//Registering the service for the notification feature
		DB.ServiceInvoke(w, countryName)
		break
	default:
		http.Error(w, "Not implemented", http.StatusNotImplemented)
	}

}

/*
This function is used to get the case information from the external API if the information dosnt already exist in the DB.
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {string} countryName is the name of the country the user wishes to search cases information on.
@param {string} url is the url used to retrieve case information from
@param {string} method is the http-methods used for http requests.
@param {bool} test is used to indicate whether the request is for testing or real purposes.
*/
func getCaseInformationFromExternalAPI(w http.ResponseWriter, countryName string, url string, method string, test bool) {
	var countryCases structures.CountryCases
	graphql := map[string]string{
		"query": `
            { 
 				 country(name: "` + countryName + `") {
					name
    				mostRecent {
      					date(format: "yyyy-MM-dd")
      					confirmed
						recovered
						deaths
						growthRate
    				}
 				 }
            }
        `,
	}

	jsonValue, _ := json.Marshal(graphql)

	request, err := utilities.ExternalRequest(w, method, url, jsonValue)
	if err != nil {
		http.Error(w, "The HTTP request failed with error:: "+err.Error(), http.StatusInternalServerError)
		return
	}

	data, _ := ioutil.ReadAll(request.Body)
	var caseCountryExternalAPI structures.CaseCountryExternalAPI

	json.Unmarshal(data, &caseCountryExternalAPI)

	if caseCountryExternalAPI.Data.Country.Name == "" {
		http.Error(w, "No information was found for the following country name/alpha-code: '"+countryName+
			"'. Are you sure the entered country name/alpha code is correct?", http.StatusNotFound)
		return
	}

	//Adding the data recieved from the external API to the
	countryCases.Country = caseCountryExternalAPI.Data.Country.Name
	countryCases.Date = caseCountryExternalAPI.Data.Country.MostRecent.Date
	countryCases.Confirmed = caseCountryExternalAPI.Data.Country.MostRecent.Confirmed
	countryCases.Deaths = caseCountryExternalAPI.Data.Country.MostRecent.Deaths
	countryCases.Recovered = caseCountryExternalAPI.Data.Country.MostRecent.Recovered
	countryCases.GrowthRate = caseCountryExternalAPI.Data.Country.MostRecent.GrowthRate

	output, err := json.Marshal(countryCases)
	if err != nil {
		fmt.Printf("Failed when encoding countryCases struct. Error: " + err.Error())
		return
	}
	//if test is false, add the countrycase to DB. This is to avoid adding test data to the DB
	if !test {
		//Storing the case into DB
		DB.AddCaseToDB(countryCases)
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, string(output))

}
