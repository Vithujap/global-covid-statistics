package handlers

import (
	"Assignment2/DB"
	"Assignment2/structures"
	"Assignment2/utilities"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

/*
This function is the handler for the policy endpoint. It only handles GET requests and retrieves policy information regarding a country from -
either DB or external API,and sends the policy information as an http response.
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {http.Request} r is the HTTP request made by a client.
*/
func PolicyHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodGet:
		//Prepare response header
		w.Header().Add("content-type", "application/json")

		//Splitting up the URL
		split := strings.Split(r.URL.Path, "/")
		alphacode := split[4]
		date := r.URL.Query().Get("scope")
		country := utilities.GetCountryNamesByAlphaCode(w, alphacode)

		//If no date is given, use the current date
		if date == "" {
			date = time.Now().Format("2006-01-02")
		}
		//Errorhandling if alphacode isnt valid
		if len(alphacode) != 3 || country == "" {
			http.Error(w, "No valid ISO 3166-1 alpha-3 country code was given. Please give a valid ISO 3166-1 alpha-3 country code", http.StatusBadRequest)
			return
		}
		//Checking if the url path matches the one that is used for testing. If not use URL for external API
		if r.URL.Path == structures.STUB_POLICY_PATH {
			fmt.Println("URL matches the one for testing. Stubbing instead of using external API")
			testURL := r.URL.String()
			getPolicyInformationFromExternalAPI(w, alphacode, testURL, date, true) //the information recieved will be from stubbed not from external api
			return
		}

		//Checking if the policy data for the given alphacode and date is already available in the DB.
		policyExists, countryPolicies := DB.CheckPolicies(alphacode, date)
		if policyExists {
			output, err := json.Marshal(countryPolicies)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Printf("Failed when encoding countryPolicies struct. Error: " + err.Error())
				return
			}
			fmt.Println("Policy found in the DB. Getting policy data from the DB")
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, string(output))
		} else {
			fmt.Println("Policy was not found in DB. Getting Policy data from external API")
			externalAPIURL := "https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/actions/" + url.PathEscape(alphacode) + "/" + url.PathEscape(date)
			getPolicyInformationFromExternalAPI(w, alphacode, externalAPIURL, date, false) //Get policy data from external API
		}

		//Registering the service for the notification feature
		DB.ServiceInvoke(w, country)
		break
	default:
		http.Error(w, "Not implemented", http.StatusNotImplemented)
	}

}

/*
This function is used to get the policy information from the external API if the information dosnt already exist in the DB.
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {string} alphacode is the ISO 3166-1 alpha-3 code of a country.
@param {string} url is the url used to retrieve policy information from
@param {string} date is the scope date for policy information
@param {bool} test is used to indicate whether the request is for testing or real purposes.
*/
func getPolicyInformationFromExternalAPI(w http.ResponseWriter, alphacode string, url string, date string, test bool) {
	//Invoke external request to the policy API to get the data
	resPolicy, err := utilities.ExternalRequest(w, http.MethodGet, url, nil)
	if err != nil {
		fmt.Errorf(err.Error())
		return
	}
	//reading data from the resPolicy http response
	policyOutput, err := ioutil.ReadAll(resPolicy.Body)
	if err != nil {
		http.Error(w, "Error in reading body. Error message: "+err.Error(), http.StatusInternalServerError)
		return
	}

	var policyData structures.PolicyData
	var countryPolicy structures.CountryPolicy

	//Decoding JSON byte array into PolicyData struct
	json.Unmarshal(policyOutput, &policyData)

	//Checking if the stringency data is available and adding the data to countryPolicy struct
	if policyData.StringencyData["msg"] != nil {
		countryPolicy.Stringency = -1
		countryPolicy.Scope = date
		countryPolicy.Country_code = strings.ToUpper(alphacode)
	} else {
		//Handling when either stingency_actual, stingency or both fields are not filled
		if policyData.StringencyData["stringency_actual"] == nil && policyData.StringencyData["stringency"] == nil {
			//if both stingency_actual and stingency isnt filled, then return -1 for the stingency value
			countryPolicy.Stringency = -1
		} else if policyData.StringencyData["stringency_actual"] == nil && policyData.StringencyData["stringency"] != nil {
			//If stingency_actual isnt filled and stingency is, stingency is used then instead of stingency_actual
			countryPolicy.Stringency = policyData.StringencyData["stringency"].(float64)
		} else {
			//Stingency_actual value is filled so that value will be used.
			countryPolicy.Stringency = policyData.StringencyData["stringency_actual"].(float64)
		}

		countryPolicy.Scope = policyData.StringencyData["date_value"].(string)
		countryPolicy.Country_code = policyData.StringencyData["country_code"].(string)
	}
	//Checking if the policyactions data is available and adding the data to countryPolicy struct
	if policyData.PolicyActions[0]["policy_type_code"] == "NONE" {
		countryPolicy.Policies = 0

	} else {
		countryPolicy.Policies = len(policyData.PolicyActions)
	}
	countryPolicy.Country_code = strings.ToUpper(alphacode)

	//Encoding countryPolicy struct into JSON byte array
	output, err := json.Marshal(countryPolicy)
	if err != nil {
		fmt.Println("Error when converting WebhookRegistration into JSON data. Error: " + err.Error())
		return
	}
	//if test is false, add the countrycase to DB. This is to avoid adding test data to the DB
	if !test {
		//Storing the policy into DB
		DB.AddPolicyToDB(countryPolicy)
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, string(output))

}
