package handlers

import (
	"Assignment2/DB"
	"Assignment2/structures"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

/*
This function is the handler for the status endpoint.
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {http.Request} r is the HTTP request made by a client.
*/
func StatusHandler(w http.ResponseWriter, r *http.Request) {
	//Prepare response header
	w.Header().Add("content-type", "application/json")
	//Invoke external request to the university API to get the data
	var status structures.Status
	graphql := map[string]string{
		"query": `
            { 
 				 country(name: "` + "Norway" + `") {
					name
    				mostRecent {
      					date(format: "yyyy-MM-dd")
      					confirmed
						recovered
						deaths
						growthRate
    				}
 				 }
            }
        `,
	}
	jsonValue, _ := json.Marshal(graphql)
	//http status code for cases API
	resCasesAPI, _ := http.Post("https://covid19-graphql.vercel.app/", "", bytes.NewBuffer(jsonValue))
	status.CasesAPI = resCasesAPI.StatusCode

	//http status code for cases API
	resPolicyAPI, _ := http.Get("https://covidtracker.bsg.ox.ac.uk/")
	status.PolicyAPI = resPolicyAPI.StatusCode

	//http status code for restcountries API
	resCountryAPI, _ := http.Get("https://restcountries.com/")
	status.RestCountriesAPI = resCountryAPI.StatusCode

	//Total webhooks
	status.Webhooks = len(DB.GetMultipleWebhooks(w))
	//Version
	status.Version = structures.AppVersion

	//time in seconds from the last service restart
	uptime := time.Since(structures.StartTime).Seconds()
	status.Uptime = uptime

	output, _ := json.Marshal(status)
	fmt.Fprintf(w, string(output))

}
