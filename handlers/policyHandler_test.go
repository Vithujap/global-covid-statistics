package handlers

import (
	"Assignment2/structures"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

/**
This function is used to test the policy endpoint. By using a URL used specifically for testing the policy path, we are able to retrieve policy information
from stubbed service and check if the gotten data matches the expected data.
*/
func TestHTTPGetPolicyRequest(t *testing.T) {
	var countryPolicy structures.CountryPolicy
	//GET request to the test policy endpoint to retrieve the stubbed data.
	request := httptest.NewRequest("GET", "http://localhost:8080"+structures.STUB_POLICY_PATH, nil)

	w := httptest.NewRecorder()
	PolicyHandler(w, request)

	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	json.Unmarshal(data, &countryPolicy)
	expectedCountryPolicy := structures.CountryPolicy{
		Country_code: "NOR",
		Scope:        "2022-01-29",
		Stringency:   45.37,
		Policies:     21,
	}

	//Testing the data gotten from the policy endpoint and checking if they match the expected data
	assert.Equal(t, expectedCountryPolicy.Country_code, countryPolicy.Country_code, "The expected and actual country code should be the same")
	assert.Equal(t, expectedCountryPolicy.Policies, countryPolicy.Policies, "The expected and actual policies should be the same")
	assert.Equal(t, expectedCountryPolicy.Scope, countryPolicy.Scope, "The expected and actual date should be the same")
	assert.Equal(t, expectedCountryPolicy.Stringency, countryPolicy.Stringency, "The expected and actual stringency should be the same")

}

/**
This function is used to test the policyhandler when wrong alphacode is given
*/
func TestPolicyHandlerWithWrongAlphacode(t *testing.T) {

	request := httptest.NewRequest("GET", "http://localhost:8080"+structures.POLICY_PATH+"Nog", nil)

	w := httptest.NewRecorder()
	PolicyHandler(w, request)

	res := w.Result()
	defer res.Body.Close()

	//Expecting that we get an 400 error and checking for it
	assert.Equal(t, http.StatusBadRequest, res.StatusCode, "The expected status code 400")

}
