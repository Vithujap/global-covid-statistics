package handlers

import (
	"Assignment2/DB"
	"Assignment2/structures"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

var webhooks structures.WebhookRegistration

/*
This function tests adding webhook to DB
*/
func TestAddNotification(t *testing.T) {
	var webhooks structures.WebhookRegistration
	DB.FirestoreInit()
	addWebhook := structures.WebhookRegistration{
		Url:     "https://localhost:8080/client/",
		Country: "Norway",
		Calls:   5,
	}
	jsonValue, _ := json.Marshal(addWebhook)
	request := httptest.NewRequest("POST", "http://localhost:8080"+structures.NOTIFICATIONS_PATH, bytes.NewBuffer(jsonValue))

	w := httptest.NewRecorder()
	NotificationHandler(w, request)

	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	json.Unmarshal(data, &webhooks)

	//Deleting the test data when the test is done
	defer DB.DeleteWebhook(w, webhooks.Webhook_id)

	//Testing the data gotten from the notification endpoint after adding data and checking if the webhook id exists
	assert.NotEqual(t, addWebhook.Webhook_id, webhooks.Webhook_id, "the expected and actual webhook ID should not be the same because the expected webhook should be empty as its manually added by us")
}

/*
This function tests deleting of a webhook
*/
func TestDeleteNotification(t *testing.T) {
	//var webhooks structures.WebhookRegistration
	DB.FirestoreInit()
	webhookID := addTestToRemove()
	request := httptest.NewRequest("DELETE", "http://localhost:8080"+structures.NOTIFICATIONS_PATH+webhookID, nil)

	w := httptest.NewRecorder()
	NotificationHandler(w, request)

	res := w.Result()
	defer res.Body.Close()

	fmt.Println(res.StatusCode)
	assert.Equal(t, http.StatusOK, res.StatusCode, "The status code should be the same as the expected if the delete worked")

}
func TestGetNotification(t *testing.T) {

}

/*
This function adds a webhook that is used by the TestDeleteNotification function
*/
func addTestToRemove() string {
	var webhooks structures.WebhookRegistration
	DB.FirestoreInit()
	addWebhook := structures.WebhookRegistration{
		Url:     "https://localhost:8080/client/",
		Country: "Norway",
		Calls:   5,
	}
	jsonValue, _ := json.Marshal(addWebhook)
	request := httptest.NewRequest("POST", "http://localhost:8080"+structures.NOTIFICATIONS_PATH, bytes.NewBuffer(jsonValue))

	w := httptest.NewRecorder()
	NotificationHandler(w, request)

	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Errorf("expected error to be nil got %v", err)
	}
	json.Unmarshal(data, &webhooks)
	return webhooks.Webhook_id
}
