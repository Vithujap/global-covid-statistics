package handlers

import (
	"Assignment2/structures"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

/*
This function is used to read the contents of a file.
@param {string} filename is the name of the file
@return []byte return the file content as an byte array
@return error returns any errors
*/
func ParseFile(filename string) ([]byte, error) {
	file, e := ioutil.ReadFile(filename)
	if e != nil {
		fmt.Printf("File error: %v\n", e)
		// Return error
		return nil, e
	}
	return file, nil
}

/*
This function is the Stubhandler for the test-case endpoint.
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {http.Request} r is the HTTP request made by a client.
*/
func StubHandlerCases(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodGet:
		log.Println("Received " + r.Method + " request on Cases stub handler. Returning mocked information.")

		// Prepare response header
		w.Header().Add("content-type", "application/json")

		// Read response content from file
		output, err := ParseFile("./res/cases.json")
		if err != nil {
			http.Error(w, "Error when reading resource file (Error: "+err.Error()+")", http.StatusInternalServerError)
			return
		}
		var caseCountryExternalAPI structures.CaseCountryExternalAPI
		json.Unmarshal(output, &caseCountryExternalAPI)
		// Write response to client
		_, err2 := fmt.Fprint(w, string(output))
		if err2 != nil {
			http.Error(w, "Error when writing HTTP response (Error: "+err2.Error()+")", http.StatusInternalServerError)
			return
		}
		break
	default:
		http.Error(w, "Handler not implemented", http.StatusNotImplemented)
	}
}

/*
This function is the Stubhandler for the test-policy endpoint.
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {http.Request} r is the HTTP request made by a client.
*/
func StubHandlerPolicy(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodGet:
		log.Println("Received " + r.Method + " request on Policy stub handler. Returning mocked information.")

		// Prepare response header
		w.Header().Add("content-type", "application/json")

		// Read response content from file
		output, err := ParseFile("./res/policy.json")
		if err != nil {
			fmt.Println("Error when reading resource file (Error: " + err.Error() + ")")
		}
		var policyData structures.PolicyData
		json.Unmarshal(output, &policyData)
		// Write response to client
		_, err2 := fmt.Fprint(w, string(output))
		if err2 != nil {
			http.Error(w, "Error when writing HTTP response (Error: "+err2.Error()+")", http.StatusInternalServerError)
			return
		}
		break
	default:
		http.Error(w, "Handler not implemented", http.StatusNotImplemented)
	}

}
