package handlers

import (
	"Assignment2/structures"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http/httptest"
	"testing"
)

/**
This function is used to test the cases endpoint. By using a URL used specirfically for testing the cases path, we are able to retrieve cases information
from stubbed service and check if the gotten data matches the expected data.
*/
func TestHTTPGetCasesRequest(t *testing.T) {
	var countryCases structures.CountryCases
	//GET request to the test cases endpoint to retrieve the stubbed data.
	request := httptest.NewRequest("GET", "http://localhost:8080"+structures.STUB_CASES_PATH, nil)

	w := httptest.NewRecorder()
	CaseHandler(w, request)

	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	json.Unmarshal(data, &countryCases)
	expectedCountryCases := structures.CountryCases{
		Country:    "Norway",
		Date:       "2022-04-05",
		Confirmed:  1411550,
		Recovered:  0,
		Deaths:     2518,
		GrowthRate: 0.0010630821154695824,
	}

	//Testing the data gotten from the policy endpoint and checking if they match the expected data
	assert.Equal(t, expectedCountryCases.Country, countryCases.Country, "The expected and actual country name should be the same")
	assert.Equal(t, expectedCountryCases.Recovered, countryCases.Recovered, "The expected and actual recovered number should be the same")
	assert.Equal(t, expectedCountryCases.Confirmed, countryCases.Confirmed, "The expected and actual confirmed number should be the same")
	assert.Equal(t, expectedCountryCases.GrowthRate, countryCases.GrowthRate, "The expected and actual growthrate number should be the same")
	assert.Equal(t, expectedCountryCases.Deaths, countryCases.Deaths, "The expected and actual death number should be the same")
	assert.Equal(t, expectedCountryCases.Date, countryCases.Date, "The expected and actual date should be the same")

}
