package handlers

import (
	"Assignment2/DB"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

/*
This function is the handler for the notification endpoint. It handles Post, Get and Delete requests of webhooks.
@param {http.ResponseWriter} w is a responsewriter used to construct a HTTP response
@param {http.Request} r is the HTTP request made by a client.
*/
func NotificationHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodPost:
		w.Header().Add("content-type", "application/json")
		DB.AddWebhooks(w, r)

	case http.MethodGet:
		w.Header().Add("content-type", "application/json")
		split := strings.Split(r.URL.Path, "/")
		webhookID := split[4]

		//If no ID is given, get all webhooks
		if len(webhookID) == 0 {
			webhooksList := DB.GetMultipleWebhooks(w)
			//Checking if there are no webhooks in the collection
			if len(webhooksList) == 0 {
				http.Error(w, "There are no registered webhooks at the moment", http.StatusNotFound)
				return
			}

			output, _ := json.Marshal(webhooksList)
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, string(output))

		} else { //Get webhook data based on the given webhook ID
			getWebhook := DB.GetSingleWebhook(w, webhookID)
			//If webhookID from the webhook struct returns empty, it got error handled
			if getWebhook.Webhook_id == "" {
				return
			}
			output, err := json.Marshal(getWebhook)
			if err != nil {
				fmt.Println("Error when converting WebhookRegistration into JSON data. Error: " + err.Error())
				return
			}

			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, string(output))
		}
	case http.MethodDelete:
		w.Header().Add("content-type", "application/json")
		split := strings.Split(r.URL.Path, "/")
		webhookID := split[4]
		//var webhook WebhookRegistration
		if webhookID == "" {
			http.Error(w, "No webhook ID was given, please give an ID in order to delete a webhook", http.StatusBadRequest)
			return
		}
		webHook := DB.GetSingleWebhook(w, webhookID)

		//if webhook ID returns empty, the webhook is not registered in the DB
		if webHook.Webhook_id == "" {
			return
		}
		DB.DeleteWebhook(w, webhookID)
		output, err := json.Marshal(webHook)
		if err != nil {
			fmt.Println("Error when converting WebhookRegistration into JSON data. Error: " + err.Error())
			return
		}
		http.Error(w, "The following webhook was deleted: "+string(output), http.StatusOK)

	default:
		http.Error(w, "Invalid method "+r.Method, http.StatusBadRequest)
	}

}
