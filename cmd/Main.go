package main

import (
	"Assignment2/DB"
	"Assignment2/handlers"
	"Assignment2/structures"
	"log"
	"net/http"
	"os"
)

func main() {
	DB.FirestoreInit()

	//Define port
	port := os.Getenv("PORT")
	if port == "" {
		log.Println("$Port has not been set. Default: 8080")
		port = "8080"
	}

	//Assign path handlers for http server

	http.HandleFunc(structures.CASES_PATH, handlers.CaseHandler)
	http.HandleFunc(structures.POLICY_PATH, handlers.PolicyHandler)
	http.HandleFunc(structures.NOTIFICATIONS_PATH, handlers.NotificationHandler)
	http.HandleFunc(structures.STATUS_PATH, handlers.StatusHandler)

	http.HandleFunc(structures.STUB_POLICY_PATH, handlers.StubHandlerPolicy)
	http.HandleFunc(structures.STUB_CASES_PATH, handlers.StubHandlerCases)
	//Start HTTP server
	log.Println("Starting server on port " + port + "...")
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal(err.Error() + "Error in creating request")
	}
	//Closing the DB
	DB.FirestoreClose()

}
