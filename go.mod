module Assignment2

go 1.15

require (
	cloud.google.com/go/firestore v1.6.1
	cloud.google.com/go/iam v0.3.0 // indirect
	firebase.google.com/go v3.13.0+incompatible
	github.com/stretchr/testify v1.7.0
	google.golang.org/api v0.70.0
)
