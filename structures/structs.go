package structures

type WebhookRegistration struct {
	Webhook_id       string `json:"webhook_id"`
	Url              string `json:"url"`
	Country          string `json:"country"`
	Calls            int    `json:"calls"`
	Registered_calls int    `json:"-"` //Used to keep track of amount of calls being invoked
}
type WebhookID struct {
	Webhook_id string `json:"webhook_id"`
}
type WebhookRegistrationWithoutRegisterede struct {
	Webhook_id string `json:"webhook_id"`
	Url        string `json:"url"`
	Country    string `json:"country"`
	Calls      int    `json:"calls"`
}
type CountryPolicy struct {
	Country_code string  `json:"country_code"`
	Scope        string  `json:"scope"`
	Stringency   float64 `json:"stringency"`
	Policies     int     `json:"policies"`
}
type PolicyData struct {
	PolicyActions  []map[string]interface{} `json:"policyActions"`
	StringencyData map[string]interface{}   `json:"stringencyData"`
}
type CountryCases struct {
	Country    string  `json:"country"`
	Date       string  `json:"date"`
	Confirmed  int     `json:"confirmed"`
	Recovered  int     `json:"recovered"`
	Deaths     int     `json:"deaths"`
	GrowthRate float64 `json:"growthRate"`
}
type CaseCountryExternalAPI struct {
	Data struct {
		Country struct {
			Name string `json:"name"`

			MostRecent struct {
				Date       string  `json:"date"`
				Confirmed  int     `json:"confirmed"`
				Recovered  int     `json:"recovered"`
				Deaths     int     `json:"deaths"`
				GrowthRate float64 `json:"growthRate"`
			} `json:"mostRecent"`
		} `json:"country"`
	} `json:"data"`
}

type Status struct {
	CasesAPI         int     `json:"cases_api"`
	PolicyAPI        int     `json:"policy_api"`
	RestCountriesAPI int     `json:"restCountries_api"`
	Webhooks         int     `json:"webhooks"`
	Version          string  `json:"version"`
	Uptime           float64 `json:"uptime"`
}
type CountryName struct {
	Name map[string]string `json:"name"`
}
