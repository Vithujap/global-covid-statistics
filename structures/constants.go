package structures

import "time"

//AppVersion
const AppVersion = "v1"

//time in seconds from the last service restart
var StartTime = time.Now()

//Purge time is in hours
const PurgeTime = 48

//URL Paths
const CASES_PATH = "/corona/" + AppVersion + "/cases/"
const POLICY_PATH = "/corona/" + AppVersion + "/policy/"
const STATUS_PATH = "/corona/" + AppVersion + "/status/"
const NOTIFICATIONS_PATH = "/corona/" + AppVersion + "/notifications/"

const STUB_POLICY_PATH = "/corona/" + AppVersion + "/policy/nor/test"
const STUB_CASES_PATH = "/corona/" + AppVersion + "/cases/Norway/test"

//firestore collection
const PoliciesCollection = "policies"
const CasesCollection = "cases"
const WebhookCollection = "webhooks"
