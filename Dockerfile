FROM golang:1.17 as builder

LABEL maintainer="vithujap@stud.ntnu.no"
LABEL stage=builder

# Set up execution environment in container's GOPATH
WORKDIR /go/src/app/cmd

# Copy relevant folders into container
COPY ./cmd /go/src/app/cmd
COPY ./handlers /go/src/app/handlers
COPY ./DB /go/src/app/DB
COPY ./res /go/src/app/res
COPY ./structures /go/src/app/structures
COPY ./utilities /go/src/app/utilities
COPY ./go.mod /go/src/app/go.mod
COPY ./go.sum /go/src/app/go.sum
COPY ./firestoreKey.json /go/src/app/firestoreKey.json
# Compile binary
RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o main




### NOW INSTANTIATING RUNTIME ENVIRONMENT

# Target container
FROM scratch

LABEL maintainer="vithujap@stud.ntnu.no"

# Root as working directory to copy compiled file to
WORKDIR /

# Retrieve binary from builder container
COPY --from=builder /go/src/app/cmd/main .
COPY --from=builder /go/src/app/firestoreKey.json ./firestoreKey.json

# Fetching the cert hints.
COPY --from=alpine:latest /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/


# Instantiate binary
CMD ["./main"]
