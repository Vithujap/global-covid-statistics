# Assignment 3
# Information
You can test the endpoints using the following link: http://34.141.84.145:8080
If you are testing the endpoints with manual deployment, you can use localhost:8080
## Deployment

### Docker-based deployment

#### Building and running server service

* `sudo docker build . -t coronaserver`

* `sudo docker run -d -p 8080:8080 coronaserver`


### Manual deployment

* Start `main.go`  


## Covid-19 Cases per Country endpoint
* Retrieve the latest number of confirmed cases and deaths for a given country, alongside growth rate of cases.
```
Method: GET 
http://34.141.84.145:8080/corona/v1/cases/{:country_name}
```
```{:country_name}``` refers to either the full name of a country or ISO 3166-1 alpha-3 country code. It is not case sensitive

* ##### Output of body example:
```
{
    "country": "Norway",
    "date": "2022-03-05",
    "confirmed": 1305006,
    "recovered": 0,
    "deaths": 1664,
    "growth_rate": 0.004199149089414866
}

```

## Covid Policy Stringency per Country endpoint
* Retrieve an overview of stringency level of policies regarding Covid-19 for a given country in a specific date. In addition to the number of currently active policies.
```
Method: GET
http://34.141.84.145:8080/corona/v1/policy/{:country_name}{?scope=YYYY-MM-DD}
```
```{:country_name}``` refers to ISO 3166-1 alpha-3 country code. It is not case sensitive
```{?scope=YYYY-MM-DD}``` refers to the date you wish to retrieve information on. This is optional, if not filled the you will retrieve the data for the current date
* ##### Output of body example:
```
{
    "country_code": "FRA",
    "scope": "2022-03-01",
    "stringency": 63.89,
    "policies": 0
}

```
* If the stringency value is -1, it means that the data for stringency isnt available yet.

## Status Interface endpoint
* Retrieve an overview of all individual servies this service depends on
```
Method: GET
http://34.141.84.145:8080/corona/v1/status/
```
* ##### Output of body example:
```
{
    "cases_api": 200,
    "policy_api": 200,
    "restCountries_api": 200,
    "webhooks": 6,
    "version": "v1",
    "uptime": 9.2048042
}

```
* ```cases_api``` shows the http status code for the *Covid 19 Cases external API
* ```policy_api``` shows the http status code for the *Corona Policy Stringency external API
* ```restCountries_api``` shows the http status code for the *restcountries external API
* ```webhooks``` shows the number of registered webhooks
* ```version``` shows the current version of the service
* ```uptime``` shows the time in seconds from the last service restart

## Notification endpoint
* Notification endpoint is a service where the user can create/view and delete webhooks that trigger a notification when the registered calls is more then or equal to minimum frequency calls. Once the condition to triggering have meet, the counter that keeps track of total service invokes get reset so that the same notification can trigger multiple times
### Registration of webhook
```
Method: POST
http://34.141.84.145:8080/corona/v1/notifications/
```
* ##### Input body example:
```
{
   "url": "https://localhost:8080/client/",
   "country": "Norway",
   "calls": 2
}
```
* ```url``` refers the URL that will be triggered upon event
* ```country``` refers to either the full name of a country or ISO 3166-1 alpha-3 country code. It is not case sensitive. The country where the trigger applies
* ```calls``` referes to the minimum number of repeated invocations before notification should occur
* ##### Output body example:
```
{
    "webhook_id": "OIdksUDwveiwe"
}
```

### Deletion of webhook
```
Method: DELETE
http://34.141.84.145:8080/corona/v1/notifications/{id}
```
* ```{id}``` refers the ID returned during the webhook registration
* ##### Output of body example:
```
The following webhook was deleted: 
{
    "webhook_id":"BJHlNxQ8CLfTwUWgDGHR",
    "url":"https://localhost:8080/client/",
    "country":"Norway",
    "calls":5
}

```

### View registered webhook
```
Method: GET
http://34.141.84.145:8080/corona/v1/notifications/{id}
```
* ```{id}``` refers the ID returned during the webhook registration
* ##### Output of body example:
```
{
    "webhook_id":"BJHlNxQ8CLfTwUWgDGHR",
    "url":"https://localhost:8080/client/",
    "country":"Norway",
    "calls":5
}
```
### View all registered webhook
```
Method: GET
http://34.141.84.145:8080/corona/v1/notifications/
```
* ##### Output of body example:
```
[{
    "webhook_id":"BJHlNxQ8CLfTwUWgDGHR",
    "url":"https://localhost:8080/client/",
    "country":"Norway",
    "calls":5
},
{
    "webhook_id":"DiSoisivucios",
    "url":"https://localhost:8080/client/",
    "country":"Denmark",
    "calls":5
},
....
]
```
## Persistance
Firestore is used to cache cases and policy data. In order to get the most recent information, the caches is purged every 48 hours.
## Testing
Currently the testings are not automated, meaning in order to run the tests, you must run main.go first then run the test files.
### Test Coverage
In order to test the coverage, run main.go first then run the following command inside the handler folder:
`go test -cover`
The test coverage is at 38.5%

